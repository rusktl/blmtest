<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function authors()
    {
        $authors = Author::with('books')->get();
        return view('authors',compact('authors'));
    }
    public function books()
    {
        $books = Book::with('author')->get();
        return view('books',compact('books'));
    }
}
