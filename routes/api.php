<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::group(['prefix' => 'v1/','as' => 'api.','namespace' => 'Api\V1','middleware' => ['auth:sanctum']], function () {
    Route::apiResource('authors', '\App\Http\Controllers\Api\V1\AuthorsController', ['except' => ['create', 'edit']]);
    Route::apiResource('books', '\App\Http\Controllers\Api\V1\BooksController', ['except' => ['create', 'edit']]);
});
